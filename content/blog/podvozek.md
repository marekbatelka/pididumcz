---
title: "Podvozek"
subtitle: Základová deska Tiny Housu
date: 2020-04-28T22:19:45+02:00
draft: true
authors: ["marek"]
categories: [""] 
tags: ["design"]
---

> A základovou desku už máš?

Začíná oblíbená otázka kamarádů. Teď už můžu říct, že mám. Co tomu ale předcházelo?

<!--more-->

## Rozhodování o velikosti

Když jsme se rozhodovali někdy v zimě 2019 o tom, že budeme stavě Tiny House, jednou z velkých otázek bylo, jaký podvozek zvolit. Na výběr byl Vlemmix z Holandska, noname podvozky různých podvalníků a nákladních podvozků, zemědělské vlečky a nebo nadměrný nehomologovaný podvozek vlastní výroby.

## Argumenty
- Neplánujeme podvozek převážet.
- Neplánujeme tedy ani tahat Tiny House za autem.
- Potřebujeme robustní podvozek, který hodně unese.
- Šířka 2,55 je pro nás spíše limitující.

Chvíli jsem uvažoval ještě o podvozku podvalníku samotného, ale to mi přišlo příliš velké a hlavně vysoké.

Nakonec padla volba na podvozek typu, v podstatě totožný co vyrábí Honza Němec z dílny [tinyhome.cz](https://tinyhome.cz/podvozek/).
Podvozek jsem si pro výrobu kreslil sám a zadal do výroby místnímu zámečníkovi. Důvodem bylo hlavně potřeba větší délky 8,5 metru.

S kvalitou zpracování jsem velmi spokojen, je o něco robustnější (použity silnější profily).
