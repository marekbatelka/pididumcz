---
title: Co se kutí na "Špicách"?
subtitle: Článek pro kalovský zpravodaj
date: 2020-06-15T20:00:00+02:00
draft: false
authors: ["marek"]
categories: [""] 
tags: ["general","preview"]
---

![Vizualizace](/img/1.PNG)

## Co je to Tiny House?
Tiny House je experimentální dřevostavba na podvozku. Rozšířená alternativa bydlení ve Spojených státech, Kanadě či Novém Zélandu si získává rychle popularitu i v dalších částech světa. Cílí na lidi co chtějí skromnost, svobodu, nízké životní náklady, jsou raději venku v přírodě než uvnitř.

## Proč Tiny House?
Tiny House je pro nás dobrodružství a experiment. Zkusíme postavit dům vlastními silami. Máme rádi výzvy a jsou nám sympatické pojmy jako minimalismus, eko bydlení, nezávislost, svoboda. Chceme si zkusit žít s málem. Tiny House se jeví jako ideální příležitost.
<!--more-->

## Jak je to velké?
Tiny House lze volně přeložit jako "malý dům". Neexistuje jasná definice, jak by Tiny House měl být veliký. Ve světě se objevují domy 3 x 2 m, ale i 12 metrů dlouhé přívesy. Náš bude stát na podvozku 8,5 x 2,5 m. Bavíme se tedy o cca 20 metrech čtverečních. To není moc, ale věříme, že chytrou dispozicí se dá komfortně žít i v takto malém prostoru. Jeden jsme navštívili na Vysočině a bylo nám v něm útulno. Interiér bude řešen jako jedna velká místnost se dvěma spacími lofty na zvýšené platformě.

## Jaká firma to staví?
Součástí dobrodružství je samozřejmě i stavba vlastníma rukama. Většinu prací máme v plánu dělat svépomocí. Informace ke stavbě jsem načerpal přes zimu z Youtube videí, na fórech a po konzultaci se kamarádem Honzou Němcem, který staví tyhle "maringotky" na zakázku. Pomáhá dobrodruhům s mentoringem a odborným vedením stavby. Občas zavolám otázku jako "Hele, mě se to štípe, kolikátkou vrtákem ten vrut předvrtáváš?" Přesně tehdy jsou rady zkušeného člověka k nezaplacení. Odborníkům přenechám to, na co si sám netroufnu - svaření podvozku, zapojení elektřiny.

## Z jakých materiálů se staví?
Snažím se přemýšlet nad stavbou ekonomicky, ale i ekologicky. Třeba velkou radost mám ze zvolené izolace. Téměř všechny materiály co použiváme jsou nějakým způsobem recyklovatelné a nebo z druhé ruky.

- Podvozek - kovový, potažený plechem
- Skladba stěny - difúzně otevřená
- Rámová konstrukce (nosná část) - dřevo - smrkové KVH profily (100 x 40 mm)
- Zateplení - konopná izolace (100mm) a dřevovláknitá deska (35mm)
- Střecha - válcovaný plech
- Okna i dveře - celodřevěné - z druhé ruky
- Vnitřní obložení - smrkové palubky
- Podlaha - modřínová opalovaná prkna

## A co vybavení interiéru?
Variant je spousta od zcela samostatných po plně připojené do všech sítí.
Náš Tiny House bude připojen na sítě. Z praktického hlediska si na tzv. "off-grid" řešení - tedy zcela soběstačné - netroufneme. Počítáme, že bude vybaven jako moderní domácnost - koupelna s pračkou, splachovacím záchodem, kuchyň s indukční deskou, myčkou, mikrovlnkou i troubou. Kvůli dětem si chceme dopřát tento komfort. 

## Kdy to bude hotové?
Pracuji příležitostě jak počasí dovolí (stavba probíhá pod širým nebem). V květnu se podařilo sestavit rámovou konstrukci a do konce června by mohla stát hrubá stavba. Celkově odhaduji práci na 500 hodin a optimisticky do zimy bydlíme. Samozřejmě předpokladem je získání územního souhlasu a všech "papírů".

## Proč Kaly?
Jsem rodák z Kalů, kde jsem do svých tří let vyrůstal. Na Kaly jsem se rád vracel na prázdniny i jindy jen tak do okolí na výlet nebo na houby. Žiju v Tišnově, pracuji v Brně. Hledáme klidné místo na venkově, stále blízko Tišnova, kde máme dobré zázemí, rodinu a několik přátel.

## Kontakt
V případě dotazů mě neváhejte kontaktovat. 

Těším se také na nová setkání.

Marek Batelka

marekbatelka@gmail.com