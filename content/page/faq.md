---
title: O projektu
subtitle: proč Tiny House
draft: true
tags: ["minimalismus", "eco", "lowfootprint"]
---

Tohle byli hlavní úvahy:

- Chceme změnu.
- Zkusíme postavit dům vlastními silami.
- Chceme trošku uniknou systému.
- Zkusíme si život v malém, hodně malém.
- Bude to experiment.
- Zkusíme si život na vesnici.
- Bude to dobrodružství.

### Historie

Vždycky jsme byli snílci s hlavou otevřenou, v oblacích nebo za horizontem. Chtěli jsme přestavět dodávku a s ní cestovat. Narodila se nám holčička, tak to chvíli nešlo. A když už to znovu šlo, přišel syn.
