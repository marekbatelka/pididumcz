---
title: "Kontakt"
date: 2020-05-01 T23:55:00+02:00
draft: false
author: ["marek"]
categories: ["general"] 
tags: ["general"]
---
V případě dotazů mě neváhejte kontaktovat. 

Těším se také na nová setkání.

Marek Batelka

marekbatelka@gmail.com