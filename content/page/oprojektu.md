---
title: O projektu
subtitle: proč Tiny House
comments: true
draft: false
tags: ["minimalismus", "eco", "lowfootprint"]
---

### Hlavní důvody
- Chceme změnu.
- Zkusíme postavit dům vlastními silami.
- Bude to dobrodružství.

### Další pro
- Zkusíme si život v malém.
- Bude to experiment.
- Zkusíme si život na vesnici.

### Historie

Vždycky jsme byli snílci s hlavou otevřenou, v oblacích nebo za horizontem. Chtěli jsme přestavět dodávku a s ní cestovat. Narodily se nám 2 děti a při hledání alternativ nás oslovit koncept Tiny House.
